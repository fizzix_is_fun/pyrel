## Creatures are active Things that can move about, participate in fighting, 
# etc. They must implement the update() function, which performs whatever state
# updates are needed each turn.

import container
import gui
from .. import thing
from .. import stats
import things.mixins.updater
import things.mixins.equipper

class Creature(thing.Thing, 
        things.mixins.updater.Updateable, things.mixins.equipper.Equipper):
    def __init__(self, gameMap, pos, name):
        thing.Thing.__init__(self, pos)
        things.mixins.updater.Updateable.__init__(self, gameMap, 
                name)
        things.mixins.equipper.Equipper.__init__(self)

        ## Start out with a bunch of default values; these should get
        # filled in as we are created by outside forces.
        self.index = None
        self.display = {}
        ## When this reaches 0 we die.
        self.curHitpoints = None

        ## Stats instance for all of our modifiable numbers.
        self.stats = stats.Stats()
        self.nativeDepth = None
        self.rarity = None
        self.experienceValue = None
        self.blows = []
        self.flags = []
        self.description = ''
        self.category = None
        
        ## Currently targeted Thing for ranged abilities.
        self.curTarget = None

        ## Maps creature names to how many of them have been killed by this 
        # specific Creature.
        self.killCountMap = {}

        ## Maps trigger conditions to lists of Procs to invoke when those 
        # triggers occur.
        self.procs = {}

        ## We'll need to have this available for AI and the like.
        self.gameMap = gameMap
        gameMap.addSubscriber(self, self.pos)
        self.resubscribe(gameMap)
        
        ## Maps our equipment slot descriptions to the items in those slots.
        # Keys are e.g. "around your neck", "on left finger", etc.
        # These are also keys in self.equipDescToSlot.
        self.equipment = container.ContainerMap()

        ## Maps different types of equipment slots to lists of descriptions
        # of slots. E.g. "finger" maps to ["on left finger", "on right finger"].
        self.equipSlotToDescs = dict()

        ## Maps descriptions of equipment slots to the specific slot type.
        # E.g. "on left finger" maps to "finger". 
        self.equipDescToSlot = dict()


    ## Add us to any appropriate Containers in the GameMap, except for the 
    # positional Container -- because our position may not be valid when
    # this function is called (e.g. because a new level has been created and
    # we're not actually on it yet).
    def resubscribe(self, gameMap):
        things.mixins.carrier.Carrier.resubscribe(self, gameMap)
        gameMap.addSubscriber(self, container.ATTACKERS)
        gameMap.addSubscriber(self, container.BLOCKERS)
        gameMap.addSubscriber(self, container.UPDATERS)
        gameMap.addSubscriber(self, container.CREATURES)


    ## Respond to an attempt by the alternate Thing to attack us.
    def receiveAttack(self, alt):
        damage = alt.getAttackDamage()
        self.curHitpoints -= damage
        gui.messenger.message("Ouch! That did %d points of damage!" % damage)
        if self.curHitpoints <= 0:
            gui.messenger.message("Oh no! I am slain!")
            self.die(alt)



    ## Get our damage for when we perform melee attacks. Attempt to use 
    # an equipped weapon, if available.
    def getAttackDamage(self):
        weapons = []
        for slot in self.equipSlotToDescs['weapon']:
            if slot in self.equipment:
                weapons.append(self.equipment[slot])

        if weapons:
            # Use weapons to attack.
            totalDamage = 0
            for weapon in weapons:
                totalDamage += weapon.getMeleeDamage()
            return totalDamage
        else:
            # Use natural attacks instead.
            return 1


    ## Respond to an attempt by the alternate Thing to move into our square. 
    # Return True if this is acceptable, False otherwise.
    # Default implementation is to reject movement attempts.
    def canMoveThrough(self, alt):
        return False


    ## Update state. For now, just wander at random.
    def update(self):
        import random
        spaces = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1],
                [1, -1], [1, 0], [1, 1]]
        random.shuffle(spaces)
        for dx, dy in spaces:
            target = (self.pos[0] + dx, self.pos[1] + dy)
            if not self.gameMap.moveMe(self, self.pos, target):
                break
        self.addEnergy(-1)


    ## Use an item.
    def useItem(self, item):
        item.onUse(self, self.gameMap)


    ## Tweak an item. Probably doesn't belong here.
    def tweakItem(self, item):
        # Prompt for this
        depth = 100
        # Apply a random theme
        lootRules = things.items.loot.LootTemplate({})
        lootRules.magicLevel = depth
#        allocator = things.items.itemAllocator.AffixAllocator(item.type, item.subtype, lootRules)
        allocator = things.items.itemAllocator.ThemeAllocator(item.type, item.subtype, lootRules, item.affixes)
        theme = allocator.allocate()
        if theme is not None:
            theme.applyTheme(depth, item)
            gui.messenger.message("added %s" % theme.name)
        else:
            gui.messenger.message("No available themes.")



    ## Remove the creature from the game.
    # \param killer The Creature responsible for killing us, if applicable.
    def die(self, killer = None):
        self.gameMap.destroy(self)
        if killer is not None:
            # Update the killer's kill count.
            if self.name not in killer.killCountMap:
                killer.killCountMap[self.name] = 1
            else:
                killer.killCountMap[self.name] += 1
            gui.messenger.message("%s has now killed %d %ss" % (killer.name, killer.killCountMap[self.name], self.name))


    ## Get the value for one of our stats.
    def getStat(self, statName):
        if self.stats.hasMod(statName):
            return self.stats.getMod(statName)
        else:
            raise RuntimeError("Creature of type %s doesn't have stat [%s]" % (self.subtype, statName))


    ## Add a stats.StatMod for the named stat.
    def addStatMod(self, statName, mod):
        self.stats.addMod(statName, mod)


    ## Clear a specific modifier for the named stat.
    def removeStatMod(self, statName, mod):
        self.stats.removeMod(statName, mod)


    ## Add an entire Stats instance to our own stats.
    def addStats(self, newStats):
        self.stats.addStats(newStats)


    ## Remove an entire Stats instance from our own stats.
    def removeStats(self, deadStats):
        self.stats.removeStats(deadStats)


    ## For debugging purposes.
    def __repr__(self):
        return "<Creature of type %s at %s>" % (self.name, self.pos)

