## A filter for valid item type/subtype selections. This assumes that a dict
# called itemType contains allowed item types as its keys (shield, sword,
# potion, hard armor etc.), and a list of allowed subtypes as its values.
# Special non-list values are "all" and "all except", which are parsed
# explicitly.
class ItemFilter:
    def __init__(self):
        # Ensure that self.itemType exists
        self.itemType = {}


    ## Return True if we are allowing this item.
    def isValidItem(self, itemType, itemSubtype):
        # No filter passed in, so anything goes.
        if not self.itemType:
            return True
        # Not valid for this type of item at all.
        if itemType not in self.itemType:
            return False
        # Valid for all subtypes of this item kind.
        if self.itemType[itemType] == "all":
            return True
        # Valid for this type, but some subtypes are specifically excluded.
        if self.itemType[itemType][0] == "all except":
            return itemSubtype not in self.itemType[itemType]
        # Valid for this type and subtype, explicitly. Or not.
        return itemSubtype in self.itemType[itemType]



## A filter for valid affix selections. As above, we assume a that a dict
# called affixType contains allowed affix types as its keys (see data file
# data/affix_meta.txt for the valid types) and a list of allowed affixes as
# its values. As above, "all" and "all except" are treated differently.
class AffixFilter:
    def __init__(self):
        # Ensure that self.affixType exists
        self.affixType = {}


    ## Return True if we are allowing this affix.
    def isValidAffix(self, affixType, affixName):
        # No filter passed in, so anything goes.
        if not self.affixType:
            return True
        # Not valid for this type of affix at all.
        if affixType not in self.affixType:
            return False
        # Valid for all affixes of this type.
        if self.affixType[affixType] == "all":
            return True
        # Valid for this type, but some affixes are specifically excluded.
        if self.affixType[affixType][0] == "all except":
            return affixName not in self.affixType[affixType]
        # Valid for this affix, explicitly. Or not.
        return affixName in self.affixType[affixType]



## A filter for valid theme selections. We assume a that a LIST
# called themes contains allowed themes. As above, "all except"
# is treated differently.
class ThemeFilter:
    def __init__(self):
        # Ensure that a list of themes exists, even if empty
        self.themes = []


    ## Return True if we are allowing this theme.
    def isValidTheme(self, themeName):
        # No filter passed in, so anything goes.
        if not self.themes:
            return True
        # Some themes are specifically excluded.
        if self.themes[0] == "all except":
            return themeName not in self.themes
        # Valid for this theme, explicitly. Or not.
        return themeName in self.themes
    