## Load item records from object.txt and object_template.txt, and create
# ItemFactories for them.

import allocatorRules
import itemFactory
import affix
import loot
import util.record
import theme

import copy
import json
import os


## Maps (type, subtype) tuples to the factories needed to instantiate them.
ITEM_TYPE_SUBTYPE_MAP = dict()
## A list of the major item types
ITEM_TYPES = []
## A map from major item types to the subtypes available for each major type
# Lazily constructed.
ITEM_SUBTYPE_MAP = dict()

## Instantiate an Item, rolling all necessary dice to randomize it.
def makeItem(key, itemLevel, gameMap, pos = None):
    if key not in ITEM_TYPE_SUBTYPE_MAP:
        raise RuntimeError("Invalid item key %s" % str(key))
    return ITEM_TYPE_SUBTYPE_MAP[key].makeItem(itemLevel, gameMap, pos)


## Directly access the factory with the given name.
def getFactory(type, subtype):
    if (type, subtype) not in ITEM_TYPE_SUBTYPE_MAP:
        raise RuntimeError("Invalid item name %s" % name)
    return ITEM_TYPE_SUBTYPE_MAP[(type, subtype)]


## Get a list of the item types, and cache it for future calls
def getTypes():
    global ITEM_TYPES
    if not ITEM_TYPES:
        uniqueTypes = set()
        for (itemType, subType) in ITEM_TYPE_SUBTYPE_MAP:
            uniqueTypes.add(itemType)
        ITEM_TYPES = list(uniqueTypes)
    return ITEM_TYPES


## Get a list of the available subtypes for a particular major type
# and cache the result for later use.
def getSubTypes(itemType):
    if not itemType in ITEM_SUBTYPE_MAP:
        subTypes = [subType for (mainType, subType) in ITEM_TYPE_SUBTYPE_MAP
                            if mainType == itemType]
        ITEM_SUBTYPE_MAP[itemType] = subTypes
    return ITEM_SUBTYPE_MAP[itemType]


## Maps template names to the factories that can apply them.
TEMPLATE_NAME_MAP = dict()
def getItemTemplate(name):
    if name not in TEMPLATE_NAME_MAP:
        raise RuntimeError("Invalid item template: [%s]" % str(name))
    return TEMPLATE_NAME_MAP[name]


## Maps affix names to the objects that can apply them.
AFFIX_NAME_MAP = dict()
def getAffix(name):
    if name not in AFFIX_NAME_MAP:
        raise RuntimeError("Invalid item affix: [%s]" % str(name))
    return AFFIX_NAME_MAP[name]


## Maps theme names to the objects that can apply them.
THEME_NAME_MAP = dict()
def getTheme(name):
    if name not in THEME_NAME_MAP:
        raise RuntimeError("Invalid item theme: [%s]" % str(name))
    return THEME_NAME_MAP[name]


## Maps template names to the factories that can apply them.
LOOT_TEMPLATE_MAP = dict()
def getLootTemplate(name):
    if name not in LOOT_TEMPLATE_MAP:
        raise RuntimeError("Invalid loot template: [%s]" % str(name))
    # Return a copy of the template to allow resolution of formulae
    return copy.deepcopy(LOOT_TEMPLATE_MAP[name])


## Returns affix minima and maxima for a given itemLevel, with caching
AFFIX_LIMITS = dict()
def getAffixLimits(itemLevel):
    # Check if we have already cached the results for this itemLevel
    if itemLevel not in AFFIX_LIMITS:
        AFFIX_LIMITS[itemLevel] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMin'] = dict()
        AFFIX_LIMITS[itemLevel]['levelsMax'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMin'] = dict()
        AFFIX_LIMITS[itemLevel]['typesMax'] = dict()
        # Iterate over all affix levels, check whether any have minima or
        # maxima at this itemLevel
        for key, level in AFFIX_LEVELS.iteritems():
            for allocation in level['allocations']:
                # Note that -1 means "no maximum"
                if (itemLevel >= allocation['minDepth'] and
                        (itemLevel <= allocation['maxDepth'] or
                        allocation['maxDepth'] is -1)):
                    # 0 is irrelevant as a minimum so we ignore it ...
                    if allocation['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['levelsMin'][key] = allocation['minNum']
                    # ... but it's a relevant maximum, so we don't
                    if allocation['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['levelsMax'][key] = allocation['minNum']
        # Now do the same for affix types
        for key, affixType in AFFIX_TYPES.iteritems():
            for allocation in affixType['allocations']:
                if (itemLevel >= allocation['minDepth'] and
                        (itemLevel <= allocation['maxDepth'] or
                        allocation['maxDepth'] is -1)):
                    if allocation['minNum'] > 0:
                        AFFIX_LIMITS[itemLevel]['typesMin'][key] = allocation['minNum']
                    if allocation['maxNum'] >= 0:
                        AFFIX_LIMITS[itemLevel]['typesMax'][key] = allocation['maxNum']
    # Return a copy so the caller can modify without affecting the cache
    return allocatorRules.AffixLimits(AFFIX_LIMITS[itemLevel])

## First load templates, so they're available when we load objects.
templates = util.record.loadRecords(os.path.join('data', 'object',
        'object_template.txt'), itemFactory.ItemFactory)
for template in templates:
    TEMPLATE_NAME_MAP[template.templateName] = template

# Now load the object records.
items = util.record.loadRecords(os.path.join('data', 'object', 'object.txt'), 
        itemFactory.ItemFactory)
for item in items:
    ITEM_TYPE_SUBTYPE_MAP[(item.type, item.subtype)] = item

# Now load the affixes.
affixes = util.record.loadRecords(os.path.join('data', 'object', 'affix.txt'),
        affix.Affix)
for affix in affixes:
    AFFIX_NAME_MAP[affix.name] = affix

# Load the affix metadata.
AFFIX_LEVELS, AFFIX_TYPES = json.load(open(os.path.join('data', 'object',
        'affix_meta.txt')))

# Now load the themes.
themes = util.record.loadRecords(os.path.join('data', 'object', 'theme.txt'),
        theme.Theme)
for theme in themes:
    THEME_NAME_MAP[theme.name] = theme

# Load the object flag metadata.
OBJECT_FLAGS = json.load(open(os.path.join('data', 'object',
        'object_flags.txt')))

# Load the loot templates.
lootTemplates = util.record.loadRecords(os.path.join('data', 'object',
        'loot_template.txt'), loot.LootTemplate)
for template in lootTemplates:
    LOOT_TEMPLATE_MAP[template.templateName] = template

#util.record.serializeRecords('object_template.txt', templates)
#util.record.serializeRecords('object.txt', items)
#util.record.serializeRecords('affix.txt', affixes)
#util.record.serializeRecords('theme.txt', themes)
#util.record.serializeRecords('loot_template.txt', lootTemplates)
