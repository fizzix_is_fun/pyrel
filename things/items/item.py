## Items are Things that you can pick up, put into containers, etc.

import container
import things.thing
import things.mixins.carrier
import things.stats
import util.grammar
import util.randomizer

class Item(things.thing.Thing, things.mixins.carrier.Carrier):
    def __init__(self, gameMap, pos = None):
        things.thing.Thing.__init__(self, pos)
        things.mixins.carrier.Carrier.__init__(self)
        ## Keep track of this for later use.
        self.gameMap = gameMap
        ## Broad typing for the object (e.g. potion, polearm, missile)
        self.type = None
        ## Base name of the object
        self.subtype = None
        ## Flavor of the object, if applicable.
        self.flavor = None
        ## Display metadata
        self.display = dict()
        ## List of procs we can trigger.
        self.procs = []
        ## List of affixes on the item (will store name, type and level).
        self.affixes = []
        ## The item's theme name
        self.theme = None
        ## Amount of the item.
        self.quantity = None
        ## Number of charges on the item.
        self.charges = None
        ## Stats instance describing our abilities.
        self.stats = things.stats.Stats()
        ## List of equip slots we can be wielded to.
        self.equipSlots = []
        ## For items that are containers, whether or not the item's contents
        # are on display.
        self.isContainerOpen = True
        ## Prose description
        self.description = ''
        ## Prefix to attach to the name, like "Diamond" or "Gloopy Green".
        self.namePrefix = None
        ## Suffix to attach to the name, like "of Slay Evil" or "of Cure Light Wounds"
        self.nameSuffix = None

    
    ## Perform any necessary initialization now that our fields have been
    # set by an outside entity.
    def init(self, gameMap):
        self.resubscribe(gameMap)
        things.mixins.carrier.Carrier.resubscribe(self, gameMap)
    

    ## Add us to appropriate containers.
    def resubscribe(self, gameMap):
        if self.pos:
            gameMap.addSubscriber(self, self.pos)
        gameMap.addSubscriber(self, container.ITEMS)
        if self.equipSlots:
            gameMap.addSubscriber(self, container.WIELDABLES)
        for proc in self.procs:
            if proc.triggerCondition == 'item use':
                gameMap.addSubscriber(self, container.USABLES)
                break


    ## Retrieve a particular stat, like whether or not we resist fire, or
    # our bonus to prowess.
    def getStat(self, statName):
        return self.stats.getMod(statName)


    ## Respond to the item being picked up.
    def onPickup(self, user, gameMap):
        self.triggerProcs(user, gameMap, 'onPickup')


    ## Respond to being dropped.
    def onDrop(self, user, gameMap):
        self.triggerProcs(user, gameMap, 'onDrop')


    ## Respond to being equipped.
    def onEquip(self, user, gameMap, equipSlot):
        self.triggerProcs(user, gameMap, 'item wield', equipSlot)
        user.addStats(self.stats)


    ## Respond to being unequipped.
    def onUnequip(self, user, gameMap):
        self.triggerProcs(user, gameMap, 'item removal')
        user.removeStats(self.stats)


    ## Return True if we are equippable.
    def canEquip(self):
        return bool(self.equipSlots)


    ## Use us -- invoke any appropriate onUse procs.
    def onUse(self, user, gameMap):
        self.triggerProcs(user, gameMap, 'item use')


    ## Return True if we have any procs that trigger on item use.
    def canUse(self):
        for proc in self.procs:
            if proc.triggerCondition == 'item use':
                return True
        return False


    ## Return True if the item is a container.
    def isContainer(self):
        return self.maxCarriedSlots or self.maxCarriedCount


    ## Toggle display of the container's contents.
    def setIsOpen(self, value):
        self.isContainerOpen = value


    ## Return True if the item is a container that is currently "open" (i.e.
    # its contents are on display.
    def isOpen(self):
        return self.isContainer() and self.isContainerOpen


    ## Trigger procs that have the specified trigger.
    def triggerProcs(self, user, gameMap, trigger, *args, **kwargs):
        for proc in self.procs:
            if proc.triggerCondition == trigger:
                proc.trigger(self, user, gameMap, *args, **kwargs)


    ## Get the damage the item would deal if used in melee -- just roll
    # its damage dice, assuming it has any.
    def getMeleeDamage(self):
        numDice = self.getStat('numDice')
        dieSize = self.getStat('dieSize')
        return util.randomizer.rollDice(numDice, dieSize)



    ## Return a string describing the item briefly -- quantity, flavor, 
    # prefix, name, suffix.
    def getShortDescription(self):
        name = ''
        if self.flavor is not None:
            # Assume that our subtype is an ability description.
            # \todo Manually adding in the article and suffix markers like this
            # seems rather hackish...
            name = util.grammar.getGrammaticalName(
                    '& %s~' % self.type.title(), self.quantity, 
                    self.flavor, 'of ' + self.subtype)
        else:
            name = util.grammar.getGrammaticalName(
                    self.subtype, self.quantity, self.namePrefix, self.nameSuffix)
        if self.isContainer():
            # Add information on how much we're carrying.
            numSlots = self.getNumUsedSlots()
            if not numSlots:
                name += ' {empty}'
            else:
                numItems = self.getNumContainedItems()
                pluralizer = ['', 's'][numSlots != 1]
                name += ' {%d slot%s, %d total}' % (numSlots, pluralizer, numItems)
        return name

            
    ## For debugging purposes, convert to string.
    def __repr__(self):
        return "<Item %s>" % (self.subtype)
